/**
 * Copyright (c) 2021 Fábio Kenji Matsuda
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package main

import (
	"errors"
	"log"
	"os"

	"database/sql"

	_ "github.com/lib/pq"
	"gitlab.com/fkmatsuda.dev/go/fk_pgmigration/migration"
)

func main() {
	args := os.Args[1:]

	if len(args) < 2 {
		log.Fatal(errors.New("Missing arguments.\nUsage:\nfk_pgmigration postgres://username:password@host[:port]/database?sslmode=mode dir[ ...]"))
	}

	db, err := sql.Open("postgres", args[0])
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	dirs := args[1:]

	err = migration.CreateMigrationSchema(db)
	if err != nil {
		log.Fatal(err)
	}

	err = migration.MigrateDirs(db, db, dirs)
	if err != nil {
		log.Fatal(err)
	}

}
