# fk_pgmigration

## Because migration should be simple...
### What does it do?
Project created to run migrations on a PostgreSQL database according to my own needs.
 
Using a PostgreSQL [driver](https://github.com/lib/pq) implemented purely in Go, other drivers for PostgreSQL or other relational databases are not included to keep the project simple and the executable size small, but you can clone the project and adapt it to your own needs. necessity.
### Usage:
~~~
fk_pgmigration conn dir[ ...]
~~~

### Parameters
**conn**
The PostgreSQL connection string, e.g.:
~~~
postgres://username:password@localhost:5432/database?sslmode=disable
~~~

**dir**
A space-separated list of directories where the migration files are located, the directories will be searched in the order given, the files will be loaded in alphabetical order.
