BEGIN;

SELECT plan(4);

SELECT has_schema('test');

SELECT has_table('test'::name, 'test'::name);

SELECT row_eq(
    $$ select id, test from test.test where id = 1 $$,
    ROW(1, 'test 1')::test.test,
    'row id 1 should be (1, ''test 1'')'
);

SELECT row_eq(
    $$ select id, test from test.test where id = 2 $$,
    ROW(2, 'test 2')::test.test,
    'row id 2 should be (2, ''test 2'')'
);

ROLLBACK;
