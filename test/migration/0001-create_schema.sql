create schema test;

create table test.test (
    id bigserial not null,
    test text not null,
    constraint pk_test primary key (id)
);
