/**
 * Copyright (c) 2021 Fábio Kenji Matsuda
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package migration

import (
	"context"
	"database/sql"
	"io/fs"
	"io/ioutil"
	"log"
	"path/filepath"
)

type migrationFile struct {
	dir      string
	fileName string
}

// Migrate runs migration of the sql files in the directories informed in dirs, the sql files must be executed in alphabetical order of their names
func MigrateDirs(db *sql.DB, roDB *sql.DB, dirs []string) (err error) {

	var files []migrationFile
	for _, dir := range dirs {
		var filesInfo []fs.FileInfo
		filesInfo, err = ioutil.ReadDir(dir)
		if err != nil {
			return
		}
		for _, fileInfo := range filesInfo {
			if !fileInfo.IsDir() {
				fileName := fileInfo.Name()
				ext := filepath.Ext(fileName)
				if ext == ".sql" {
					files = append(files, migrationFile{
						dir:      dir,
						fileName: fileName,
					})
				}
			}
		}
	}

	files, err = filterMissing(roDB, files)

	for _, file := range files {
		var fileBytes []byte
		fileName := file.fileName
		fileBytes, err = ioutil.ReadFile(filepath.Join(file.dir, fileName))
		if err != nil {
			return
		}
		err = MigrateScript(db, fileName, string(fileBytes))
		if err != nil {
			return
		}
	}

	return

}

func MigrateScript(db *sql.DB, name, sql string) (err error) {
	err = db.Ping()
	if err != nil {
		return err
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			log.Default().Println("Rollback...")
			log.Default().Printf("Error executing migration: \"%s\"\n", name)
			return
		}
		err = tx.Commit()
	}()

	_, err = tx.Exec(sql)
	if err != nil {
		return
	}

	err = registerScript(tx, name)

	return
}

// CreateMigrationSchema creates, if it does not exist, the schema to register already executed scripts
func CreateMigrationSchema(db *sql.DB) (err error) {

	err = db.Ping()
	if err != nil {
		return
	}

	_, err = db.Exec(`
        create schema if not exists fk_migration;
        create table if not exists fk_migration.migrations (
            filename text not null,
            executed_at timestamp not null default now(),
            constraint pk_migrations primary key (filename)
        );
    `)
	return
}

func filterMissing(roDB *sql.DB, files []migrationFile) (missingFiles []migrationFile, err error) {

	st, err := roDB.Prepare("select count(*) from fk_migration.migrations where filename = $1")
	if err != nil {
		return
	}

	for _, file := range files {
		var r *sql.Rows
		fileName := file.fileName
		r, err = st.Query(fileName)
		if err != nil {
			return
		}
		defer r.Close()
		var rc uint32
		if r.Next() {
			err = r.Scan(&rc)
			if err != nil {
				return
			}
			if rc == 0 {
				missingFiles = append(missingFiles, file)
			}
		}

	}

	return
}

func registerScript(tx *sql.Tx, name string) (err error) {
	st, err := tx.Prepare("insert into fk_migration.migrations (filename) values ($1)")
	if err != nil {
		return
	}
	defer st.Close()

	_, err = st.Exec(name)
	return

}
